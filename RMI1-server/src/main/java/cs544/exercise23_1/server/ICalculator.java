package cs544.exercise23_1.server;

public interface ICalculator {
	
	public double Calculate(int operator, int a, int b);
}
